<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title></title>
  </head>
  <body>
    <div class="text">

      <input type="checkbox" id="modifier"><label for="modifier">Modifier</label>

      <?php

          ini_set("display_errors","true");

          $handle = mysqli_connect("localhost","root","","PenduApp");
          $query = "SELECT * FROM mots";
          $result = mysqli_query($handle,$query);
          echo "Il y a " . $result->num_rows . " mots dans la liste." . "<br>";
          echo "<ul>";
          while($line = mysqli_fetch_array($result)) {
              echo "<li>" . $line["id"] . " " . $line["mot"];
              echo "<a class=\"supression modif\" href=\"delete_mot.php?id=" . $line["id"] . "\">Supprimer</a>";
              echo "<form class=\"modif\" action=\"modifier_mot.php?id=" . $line["id"] . "\" method=\"post\" >";
              echo "<input type=\"text\" name=\"update\" placeholder=\"Modifier\">";
              echo "<input type=\"submit\">";
              echo "</form>" . "</li>";
          }
          echo "</ul>";

      ?>

      <form class="ajout modif" action="create_mot.php" method="post">
        <input type="text" name="mot" placeholder="Nouveau mot">
        <input type="submit">
      </form>

      <hr>

      <?php

          $query = "SELECT * FROM joueurs";
          $result = mysqli_query($handle,$query);
          echo "Il y a " . $result->num_rows . " joueurs en lice." . "<br>";
          echo "<ul>";
          while($line = mysqli_fetch_array($result)) {
              echo "<li>" . $line["id"] . " " . $line["nom"];
              echo "<a class=\"supression modif\" href=\"delete_joueur.php?id=" . $line["id"] . "\">Supprimer</a>";
              echo "<form class=\"modif\" action=\"modifier_joueur.php?id=" . $line["id"] . "\" method=\"post\" >";
              echo "<input type=\"text\" name=\"update\" placeholder=\"Modifier\">";
              echo "<input type=\"submit\">";
              echo "</form>" . "</li>";
          }
          echo "</ul>";

      ?>

      <form class="ajout modif" action="create_joueur.php" method="post">
        <input type="text" name="joueur" placeholder="Nouveau joueur">
        <input type="submit">
      </form>

    </div>
  </body>
</html>
